#![no_std]

extern crate serde;
#[macro_use]
extern crate serde_derive;

#[cfg(feature = "alloc")]
extern crate alloc;

extern crate heapless;

use heapless::{Vec, ArrayLength, consts::*};

use postcard::{from_bytes, to_vec};
#[cfg(feature = "alloc")]
use postcard::{to_allocvec};

pub type TxBuffer = Vec<u8, U104>;
pub type RxBuffer = Vec<u8, U10>;
pub type FrameBuffer = Vec<u8, U1000>;



#[derive(Deserialize, Serialize, Debug)]
pub struct Tx
{
    pub sample_time: f32,
    pub samples: Vec<f32, U100>
}

impl Tx
{
    pub fn serialize<NOUT>(tx: &Tx) -> Result<Vec<u8, NOUT>, postcard::Error>
        where NOUT: ArrayLength<u8>
    {
        to_vec(tx)
    }

    #[cfg(feature = "alloc")]
    pub fn serialize_alloc(tx: &Tx) -> Result<alloc::vec::Vec<u8>, postcard::Error>
    {
        to_allocvec(tx)
    }

    pub fn deserialize(tx: &[u8]) -> Result<Tx, postcard::Error> {
        from_bytes(tx)
    }
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Rx {

}

#[derive(Debug, Deserialize, Serialize)]
pub enum Frame {
    ETHERNET(FrameBuffer),
    TX(TxBuffer),
    RX(RxBuffer)
}

#[cfg(test)]
mod tests {

    use crate::Tx;
    use heapless::{Vec, consts::*};

    #[test]
    fn serde_tx() {

        let mut data: Vec<f32, U100> = Vec::new();
        data.push(1.0).unwrap();
        data.push(10.0).unwrap();
        data.push(5.0).unwrap();

        let original = Tx {
            samples: data,
            sample_time: 1e6,
        };

        let serialized: Vec<u8, U104> = Tx::serialize(&original).unwrap();

        let deserialized = Tx::deserialize(&serialized[..]).unwrap();
        assert_eq!(deserialized.sample_time, 1e6f32);
        assert_eq!(deserialized.samples, [1.0, 10.0, 5.0]);
    }
}